package com.iskae.neoweather.di;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.iskae.neoweather.data.source.local.WeatherDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by iskae on 07.03.18.
 */

@Module(includes = ViewModelModule.class)
public class ApplicationModule {
  @NonNull
  private Application application;

  public ApplicationModule(@NonNull Application app) {
    this.application = app;
  }

  @Singleton
  @Provides
  public Application provideApplication() {
    return application;
  }

  @Singleton
  @Provides
  public Context provideContext() {
    return application.getApplicationContext();
  }

  @Singleton
  @Provides
  public SharedPreferences providesSharedPreferences(@NonNull Application application) {
    return PreferenceManager.getDefaultSharedPreferences(application);
  }

  @Singleton
  @Provides
  public WeatherDatabase providesWeatherDatabase(Application app) {
    return Room.databaseBuilder(app, WeatherDatabase.class, "weather")
        .build();
  }
}
