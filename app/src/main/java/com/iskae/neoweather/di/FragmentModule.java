package com.iskae.neoweather.di;

import com.iskae.neoweather.search.SearchLocationsFragment;
import com.iskae.neoweather.weather.WeatherFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by iskae on 07.03.18.
 */

@Module
abstract class FragmentModule {

  @ContributesAndroidInjector
  abstract SearchLocationsFragment contributesSearchLocationsFragment();

  @ContributesAndroidInjector
  abstract WeatherFragment contributesWeatherFragment();

}
