package com.iskae.neoweather.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.iskae.neoweather.util.ViewModelFactory;
import com.iskae.neoweather.util.SharedViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@SuppressWarnings("WeakerAccess")
@Module
public abstract class ViewModelModule {

  @Binds
  @IntoMap
  @ViewModelKey(SharedViewModel.class)
  abstract ViewModel bindSearchViewModel(SharedViewModel searchViewModel);

  @Binds
  abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
