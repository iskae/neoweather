package com.iskae.neoweather.di;

import com.iskae.neoweather.util.NeoWeatherApplication;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by iskae on 07.03.18.
 */
@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class,
    FragmentModule.class, ActivitiesModule.class})
public interface ApplicationComponent {
  void inject(NeoWeatherApplication neoWeatherApplication);
}
