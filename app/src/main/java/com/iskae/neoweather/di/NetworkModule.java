package com.iskae.neoweather.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.app.Application;

import com.iskae.neoweather.R;
import com.iskae.neoweather.data.source.remote.WeatherService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by iskae on 07.03.18.
 */

@Module
public class NetworkModule {

  @Singleton
  @Provides
  public Gson provideGson() {
    return new GsonBuilder()
        .create();
  }

  @Singleton
  @Provides
  public OkHttpClient provideOkHttpClient(HttpLoggingInterceptor interceptor) {
    return new OkHttpClient.Builder()
        .retryOnConnectionFailure(true)
        .addInterceptor(interceptor)
        .build();
  }

  @Singleton
  @Provides
  public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
    return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC);
  }

  @Singleton
  @Provides
  public WeatherService provideWeatherService(Application application, OkHttpClient client, Gson gson) {
    return new Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(application.getString(R.string.base_url_owm))
        .client(client)
        .build()
        .create(WeatherService.class);
  }
}
