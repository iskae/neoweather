package com.iskae.neoweather.di;

import com.iskae.neoweather.search.SearchLocationsActivity;
import com.iskae.neoweather.weather.WeatherActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by iskae on 07.03.18.
 */

@Module
abstract class ActivitiesModule {
  @ContributesAndroidInjector
  abstract SearchLocationsActivity contributesSearchLocationsActivity();

  @ContributesAndroidInjector
  abstract WeatherActivity contributesWeatherActivity();

}
