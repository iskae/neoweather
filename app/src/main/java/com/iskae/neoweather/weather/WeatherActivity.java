package com.iskae.neoweather.weather;

import static com.iskae.neoweather.search.SearchLocationsActivity.EXTRA_CITY;
import static com.iskae.neoweather.search.SearchLocationsActivity.WEATHER_FRAGMENT_TAG;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.iskae.neoweather.R;
import com.iskae.neoweather.data.model.City;
import com.iskae.neoweather.databinding.ActivityWeatherBinding;
import com.iskae.neoweather.util.BaseActivity;

/**
 * Created by iskae on 08.03.18.
 */

public class WeatherActivity extends BaseActivity {

  private ActivityWeatherBinding binding;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.setContentView(this, R.layout.activity_weather);
    setHomeEnabled(true);

    Intent intent = getIntent();
    City city = intent.getParcelableExtra(EXTRA_CITY);
    if (city != null && savedInstanceState == null) {
      setActionBarTitle(city.toString());
      FragmentManager manager = getSupportFragmentManager();
      WeatherFragment fragment = (WeatherFragment) manager.findFragmentByTag(WEATHER_FRAGMENT_TAG);
      if (fragment == null) {
        fragment = WeatherFragment.newInstance(city);
      }
      addFragmentToActivity(manager, fragment, R.id.weatherFragmentContainer, WEATHER_FRAGMENT_TAG);
    }
  }
}
