package com.iskae.neoweather.weather;

import static com.iskae.neoweather.search.SearchLocationsActivity.EXTRA_CITY;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.iskae.neoweather.R;
import com.iskae.neoweather.data.model.City;
import com.iskae.neoweather.data.model.WeatherData;
import com.iskae.neoweather.databinding.FragmentWeatherBinding;
import com.iskae.neoweather.util.BaseFragment;
import com.iskae.neoweather.util.SharedViewModel;
import com.squareup.picasso.Picasso;

import java.net.UnknownHostException;

import timber.log.Timber;

/**
 * Created by iskae on 08.03.18.
 */

public class WeatherFragment extends BaseFragment {

  SharedViewModel sharedViewModel;

  private FragmentWeatherBinding binding;
  private City city;

  public WeatherFragment() {

  }

  public static WeatherFragment newInstance(City city) {
    WeatherFragment fragment = new WeatherFragment();
    Bundle args = new Bundle();
    args.putParcelable(EXTRA_CITY, city);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    Bundle args = getArguments();
    if (args != null) {
      city = args.getParcelable(EXTRA_CITY);
    }
    if (city != null) {
      sharedViewModel = getViewModel(SharedViewModel.class);
      sharedViewModel.loadWeatherByCityId(city.getId());
      observeLoadingStatus();
      observeResponse();
      observeError();
    }
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weather, container, false);
    View rootView = binding.getRoot();
    return rootView;
  }

  private void observeResponse() {
    sharedViewModel.getCurrentWeather().observe(this, this::processCurrentWeather);
  }

  private void observeError() {
    sharedViewModel.getError().observe(this, this::processError);
  }

  private void observeLoadingStatus() {
    sharedViewModel.getLoadingStatus().observe(this, this::processLoadingStatus);
  }

  private void processLoadingStatus(boolean isLoading) {
    binding.weatherLoadingLayout.setLoading(isLoading);
  }

  private void processCurrentWeather(WeatherData weather) {
    binding.weatherLoadingLayout.setLoading(false);
    if (weather != null) {
      binding.setWeatherData(weather);
      Picasso.get().load(getString(R.string.icon_url, weather.getWeather().get(0).getIcon())).into(binding.weatherIconView);
      binding.weatherContentView.setVisibility(View.VISIBLE);
      binding.weatherEmptyLayout.emptyView.setVisibility(View.GONE);
    } else {
      binding.weatherContentView.setVisibility(View.GONE);
      binding.weatherEmptyLayout.emptyView.setVisibility(View.VISIBLE);
      binding.weatherEmptyLayout.emptyTextView.setText(R.string.no_data_found);
      binding.weatherEmptyLayout.refreshDataButton.setOnClickListener(v -> {
            binding.weatherEmptyLayout.emptyView.setVisibility(View.GONE);
            sharedViewModel.loadWeatherByCityId(city.getId());
          }
      );
    }
  }

  private void processError(Throwable error) {
    if (error instanceof UnknownHostException) {
      binding.weatherLoadingLayout.loadingTextView.setText(R.string.getting_last_known_weather);
      sharedViewModel.loadLastKnownWeatherByCityId(city.getId());
    } else {
      Timber.e(error);
      Toast.makeText(getContext(), R.string.error_retrieving_current_weather, Toast.LENGTH_SHORT).show();
    }
  }
}
