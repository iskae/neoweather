package com.iskae.neoweather.binding;

import android.databinding.BindingAdapter;
import android.view.View;

/**
 * Custom BindingAdapter class
 */
public class BindingAdapters {

  /**
   * Toogle view visibility
   * @param view view, which the visibility property be toggled
   * @param show boolean, visibility value
   */
  @BindingAdapter("showView")
  public static void showView(View view, boolean show) {
    view.setVisibility(show ? View.VISIBLE : View.GONE);
  }
}
