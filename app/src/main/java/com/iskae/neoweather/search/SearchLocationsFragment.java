package com.iskae.neoweather.search;

import android.app.SearchManager;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.iskae.neoweather.R;
import com.iskae.neoweather.data.model.City;
import com.iskae.neoweather.databinding.FragmentSearchLocationsBinding;
import com.iskae.neoweather.util.BaseFragment;
import com.iskae.neoweather.util.SharedViewModel;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;


/**
 * Created by iskae on 07.03.18.
 */

public class SearchLocationsFragment extends BaseFragment {
  public interface OnCityClickedListener {
    void onCityClicked(City city);
  }

  SharedViewModel sharedViewModel;

  private FragmentSearchLocationsBinding binding;
  private SearchView searchView;
  private CitiesListAdapter adapter;
  private OnCityClickedListener listener;

  public SearchLocationsFragment() {

  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    try {
      listener = (OnCityClickedListener) context;
    } catch (ClassCastException e) {
      throw new ClassCastException(context.toString()
          + " must implement OnCityClickListener");
    }
  }

  public static SearchLocationsFragment newInstance() {
    return new SearchLocationsFragment();
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    setRetainInstance(true);
    sharedViewModel = getViewModel(SharedViewModel.class);
    adapter = new CitiesListAdapter(getContext(), new ArrayList<>(), listener);
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    sharedViewModel.loadCitiesList();
    ((AppCompatActivity) getActivity()).setSupportActionBar(binding.toolbar);
    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.cities);
    observeLoadingStatus();
    observeFilter();
    observeResponse();
    observeError();
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_locations, container, false);
    View rootView = binding.getRoot();
    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(),
        getResources().getInteger(R.integer.column_count));
    binding.citiesListView.setLayoutManager(layoutManager);
    //Actually this animation should not work, because notifyDatasetChanged does not trigger recyclerview animations
    SlideInUpAnimator animator = new SlideInUpAnimator(new OvershootInterpolator(1f));
    binding.citiesListView.setItemAnimator(animator);
    binding.citiesListView.setAdapter(adapter);
    return rootView;
  }

  private void observeFilter() {
    sharedViewModel.getFilter().observe(this, this::processFilter);
  }

  private void processFilter(String filter) {
    adapter.getFilter().filter(filter);
  }

  private void observeResponse() {
    sharedViewModel.getCitiesList().observe(this, this::processList);
  }

  private void observeError() {
    sharedViewModel.getError().observe(this, this::processError);
  }

  private void observeLoadingStatus() {
    sharedViewModel.getLoadingStatus().observe(this, this::processLoadingStatus);
  }

  private void processLoadingStatus(boolean isLoading) {
    binding.setLoading(isLoading);
    binding.searchLoadingLayout.setLoading(isLoading);
  }

  private void processList(List<City> list) {
    if (list != null && list.size() > 0) {
      binding.setLoading(false);
      binding.searchLoadingLayout.setLoading(false);
      adapter.setCitiesList(list);
    } else {
      binding.citiesListView.setVisibility(View.GONE);
      binding.searchLoadingLayout.loadingTextView.setText(R.string.generating_cities_list);
    }
  }

  private void processError(Throwable error) {
    Timber.e(error);
    Toast.makeText(getContext(), R.string.error_generate_cities, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.menu_search_locations, menu);
    SearchLocationsActivity activity = (SearchLocationsActivity) getActivity();
    if (activity != null) {
      SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
      searchView = (SearchView) menu.findItem(R.id.action_search)
          .getActionView();
      searchView.setSearchableInfo(searchManager
          .getSearchableInfo(activity.getComponentName()));
      searchView.setMaxWidth(Integer.MAX_VALUE);

      searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
          sharedViewModel.setFilter(query);
          return true;
        }

        @Override
        public boolean onQueryTextChange(String query) {
          sharedViewModel.setFilter(query);
          return true;
        }
      });
    }
  }
}
