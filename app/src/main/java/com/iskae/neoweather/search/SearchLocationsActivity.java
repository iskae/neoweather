package com.iskae.neoweather.search;

import android.app.ActivityOptions;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.iskae.neoweather.R;
import com.iskae.neoweather.data.model.City;
import com.iskae.neoweather.databinding.ActivitySearchLocationsBinding;
import com.iskae.neoweather.util.BaseActivity;
import com.iskae.neoweather.util.SharedViewModel;
import com.iskae.neoweather.weather.WeatherActivity;
import com.iskae.neoweather.weather.WeatherFragment;

import javax.inject.Inject;

public class SearchLocationsActivity extends BaseActivity implements SearchLocationsFragment.OnCityClickedListener {
  private static final String SEARCH_LOCATIONS_FRAGMENT_TAG = "SEARCH_LOCATIONS_FRAGMENT_TAG";
  public static final String EXTRA_CITY = "EXTRA_CITY";
  public static final String WEATHER_FRAGMENT_TAG = "WEATHER_FRAGMENT_TAG";
  @Inject
  ViewModelProvider.Factory viewModelFactory;
  private ActivitySearchLocationsBinding binding;
  private SharedViewModel sharedViewModel;
  private FragmentManager manager;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.setContentView(this, R.layout.activity_search_locations);
    sharedViewModel = ViewModelProviders.of(this, viewModelFactory).get(SharedViewModel.class);

    if (savedInstanceState == null) {
      manager = getSupportFragmentManager();
      SearchLocationsFragment fragment = (SearchLocationsFragment) manager.findFragmentByTag(SEARCH_LOCATIONS_FRAGMENT_TAG);
      if (fragment == null) {
        fragment = SearchLocationsFragment.newInstance();
      }
      addFragmentToActivity(manager, fragment, R.id.searchLocationsFragmentContainer, SEARCH_LOCATIONS_FRAGMENT_TAG);
    }
  }

  @Override
  public void onCityClicked(City city) {
    boolean twoPane = getResources().getBoolean(R.bool.twoPane);
    if (twoPane) {
      WeatherFragment weatherFragment = (WeatherFragment) manager.findFragmentByTag(WEATHER_FRAGMENT_TAG);
      if (weatherFragment == null) {
        weatherFragment = WeatherFragment.newInstance(city);
      }
      addFragmentToActivity(manager, weatherFragment, R.id.searchLocationsFragmentContainer, SEARCH_LOCATIONS_FRAGMENT_TAG);
    } else {
      Intent intent = new Intent(this, WeatherActivity.class);
      intent.putExtra(EXTRA_CITY, city);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        startActivity(intent,
            ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
      } else {
        startActivity(intent);

      }
    }
  }
}
