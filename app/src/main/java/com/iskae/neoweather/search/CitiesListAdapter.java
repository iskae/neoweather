package com.iskae.neoweather.search;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.iskae.neoweather.data.model.City;
import com.iskae.neoweather.databinding.CitiesListItemBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by iskae on 07.03.18.
 */

public class CitiesListAdapter extends RecyclerView.Adapter<CitiesListAdapter.ViewHolder> implements Filterable {
  private List<City> citiesList;
  private List<City> filteredCitiesList;
  private Context context;
  private SearchLocationsFragment.OnCityClickedListener cityClickedListener;

  CitiesListAdapter(Context context, List<City> cities, SearchLocationsFragment.OnCityClickedListener cityClickedListener) {
    this.context = context;
    this.citiesList = cities;
    this.filteredCitiesList = cities;
    this.cityClickedListener = cityClickedListener;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    CitiesListItemBinding binding = CitiesListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
    return new ViewHolder(binding);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    City city = filteredCitiesList.get(position);
    if (city != null) {
      holder.binding.cityName.setText(city.toString());
      holder.binding.getRoot().setOnClickListener(v -> cityClickedListener.onCityClicked(city));
    }
  }

  @Override
  public int getItemCount() {
    if (filteredCitiesList == null) return 0;
    return filteredCitiesList.size();
  }

  public void setCitiesList(List<City> citiesList) {
    if (filteredCitiesList == null || filteredCitiesList.size() == 0) {
      this.citiesList = this.filteredCitiesList = citiesList;
      notifyDataSetChanged();
    }
  }

  @Override
  public Filter getFilter() {
    return new Filter() {
      @Override
      protected FilterResults performFiltering(CharSequence charSequence) {
        String charString = charSequence.toString();
        if (charString.isEmpty()) {
          filteredCitiesList = citiesList;
        } else {
          List<City> filteredList = new ArrayList<>();
          for (City city : citiesList) {
            if (city.getCityName().toLowerCase().contains(charString.toLowerCase())) {
              filteredList.add(city);
            }
          }
          filteredCitiesList = filteredList;
        }

        FilterResults filterResults = new FilterResults();
        filterResults.values = filteredCitiesList;
        return filterResults;
      }

      @Override
      protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        filteredCitiesList = (ArrayList<City>) filterResults.values;
        notifyDataSetChanged();
      }
    };
  }


  public class ViewHolder extends RecyclerView.ViewHolder {
    private final CitiesListItemBinding binding;

    public ViewHolder(CitiesListItemBinding binding) {
      super(binding.getRoot());
      this.binding = binding;
    }
  }
}
