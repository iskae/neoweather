package com.iskae.neoweather.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by iskae on 08.03.18.
 */

public class Wind {
  @SerializedName("speed")
  private double speed;
  @SerializedName("deg")
  private double deg;

  public double getSpeed() {
    return speed;
  }

  public void setSpeed(double speed) {
    this.speed = speed;
  }

  public double getDeg() {
    return deg;
  }

  public void setDeg(double deg) {
    this.deg = deg;
  }
}
