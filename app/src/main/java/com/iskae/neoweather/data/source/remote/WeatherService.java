package com.iskae.neoweather.data.source.remote;

import com.iskae.neoweather.data.model.WeatherData;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by iskae on 07.03.18.
 */

public interface WeatherService {
  @GET("weather")
  Flowable<WeatherData> getCurrentWeatherForCoordinates(@Query("appid") String apiKey, @Query("id") String cityId);
}
