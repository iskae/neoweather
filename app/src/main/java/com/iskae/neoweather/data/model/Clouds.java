package com.iskae.neoweather.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by iskae on 08.03.18.
 */

public class Clouds {
  @SerializedName("all")
  private int all;

  public int getAll() {
    return all;
  }

  public void setAll(int all) {
    this.all = all;
  }
}
