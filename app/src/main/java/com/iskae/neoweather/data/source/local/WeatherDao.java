package com.iskae.neoweather.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.iskae.neoweather.data.model.WeatherData;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * Created by iskae on 07.03.18.
 */
@Dao
public interface WeatherDao {
  @Query("SELECT * FROM WeatherData WHERE id == :cityId")
  Maybe<WeatherData> getLastKnownWeather(long cityId);

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void insertWeather(WeatherData weather);
}
