package com.iskae.neoweather.data.source.local;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.content.Context;

import com.iskae.neoweather.R;
import com.iskae.neoweather.data.model.City;
import com.iskae.neoweather.data.model.WeatherData;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * Created by iskae on 09.03.18.
 */

public class LocalWeatherRepository {
  private Context context;
  private WeatherDatabase weatherDatabase;
  private Gson gson;

  @Inject
  public LocalWeatherRepository(Context context, WeatherDatabase weatherDatabase, Gson gson) {
    this.context = context;
    this.weatherDatabase = weatherDatabase;
    this.gson = gson;
  }

  public void generateCitiesList() {
    InputStream raw = context.getResources().openRawResource(R.raw.cities);
    Reader reader = new BufferedReader(new InputStreamReader(raw));
    List<City> cities = gson.fromJson(reader, new TypeToken<List<City>>() {
    }.getType());
    weatherDatabase.cityDao().insertCities(cities);
  }

  public Flowable<List<City>> getCitiesList() {
    return weatherDatabase.cityDao().getCitiesList();
  }

  public Maybe<WeatherData> getLastKnownWeatherByCityId(long cityId) {
    return weatherDatabase.weatherDao().getLastKnownWeather(cityId);
  }

  public void saveWeather(WeatherData weather) {
    weatherDatabase.weatherDao().insertWeather(weather);
  }
}
