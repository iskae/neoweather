package com.iskae.neoweather.data.model;

import com.google.gson.annotations.SerializedName;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.text.format.DateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by iskae on 07.03.18.
 */

@Entity
public class WeatherData {
  @SerializedName("coord")
  @Embedded
  private Coord coord;
  @SerializedName("weather")
  private List<Weather> weather = new ArrayList<>();
  @SerializedName("base")
  private String base;
  @SerializedName("main")
  @Embedded
  private Main main;
  @SerializedName("wind")
  @Embedded
  private Wind wind;
  @SerializedName("clouds")
  @Embedded
  private Clouds clouds;
  @SerializedName("dt")
  private int dt;
  @SerializedName("sys")
  @Embedded
  private Sys sys;
  @PrimaryKey
  @SerializedName("id")
  private int id;
  @SerializedName("name")
  private String name;
  @SerializedName("cod")
  private int cod;

  public Coord getCoord() {
    return coord;
  }

  public void setCoord(Coord coord) {
    this.coord = coord;
  }

  public List<Weather> getWeather() {
    return weather;
  }

  public void setWeather(List<Weather> weather) {
    this.weather = weather;
  }

  public String getBase() {
    return base;
  }

  public void setBase(String base) {
    this.base = base;
  }

  public Main getMain() {
    return main;
  }

  public void setMain(Main main) {
    this.main = main;
  }

  public Wind getWind() {
    return wind;
  }

  public void setWind(Wind wind) {
    this.wind = wind;
  }

  public Clouds getClouds() {
    return clouds;
  }

  public void setClouds(Clouds clouds) {
    this.clouds = clouds;
  }

  public int getDt() {
    return dt;
  }

  public void setDt(int dt) {
    this.dt = dt;
  }

  public Sys getSys() {
    return sys;
  }

  public void setSys(Sys sys) {
    this.sys = sys;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getCod() {
    return cod;
  }

  public void setCod(int cod) {
    this.cod = cod;
  }

  public String getTime(long timestamp) {
    Date date = new Date(timestamp * 1000);
    return (String) DateFormat.format("HH:mm", date);
  }

  public String getWindDegreeAsString(double degree) {
    String directions[] = {"N", "NE", "E", "SE", "S", "SW", "W", "NW", "N"};
    return directions[(int) Math.round(((degree % 360) / 45))];
  }

  public String getTemperatureString(double kelvinValue) {
    double celcius = kelvinValue - 273.15;
    return roundDouble(celcius) + "\u00b0";
  }

  public int roundDouble(double value) {
    return (int) Math.round(value);
  }
}
