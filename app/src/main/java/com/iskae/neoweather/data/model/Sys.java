package com.iskae.neoweather.data.model;

import com.google.gson.annotations.SerializedName;

import android.arch.persistence.room.Ignore;

/**
 * Created by iskae on 08.03.18.
 */

public class Sys {
  @SerializedName("type")
  private int type;
  @SerializedName("id")
  @Ignore
  private int id;
  @SerializedName("message")
  private double message;
  @SerializedName("country")
  private String country;
  @SerializedName("sunrise")
  private int sunrise;
  @SerializedName("sunset")
  private int sunset;

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public double getMessage() {
    return message;
  }

  public void setMessage(double message) {
    this.message = message;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public int getSunrise() {
    return sunrise;
  }

  public void setSunrise(int sunrise) {
    this.sunrise = sunrise;
  }

  public int getSunset() {
    return sunset;
  }

  public void setSunset(int sunset) {
    this.sunset = sunset;
  }
}
