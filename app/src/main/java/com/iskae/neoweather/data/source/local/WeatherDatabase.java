package com.iskae.neoweather.data.source.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.iskae.neoweather.data.model.City;
import com.iskae.neoweather.data.model.WeatherData;

/**
 * Created by iskae on 07.03.18.
 */
@Database(entities = {City.class, WeatherData.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class WeatherDatabase extends RoomDatabase {
  public abstract WeatherDao weatherDao();

  public abstract CityDao cityDao();
}
