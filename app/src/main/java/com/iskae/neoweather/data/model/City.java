package com.iskae.neoweather.data.model;

import com.google.gson.annotations.SerializedName;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by iskae on 08.03.18.
 */

@Entity
public class City implements Parcelable {

  public static final Parcelable.Creator<City> CREATOR = new Parcelable.Creator<City>() {
    @Override
    public City createFromParcel(Parcel source) {
      return new City(source);
    }

    @Override
    public City[] newArray(int size) {
      return new City[size];
    }
  };

  @PrimaryKey
  @SerializedName("id")
  private long id;
  @SerializedName("name")
  private String cityName;
  @SerializedName("country")
  private String countryCode;
  @SerializedName("lat")
  private String latitude;
  @SerializedName("lon")
  private String longitude;

  public City() {

  }

  public City(Parcel in) {
    this.id = in.readLong();
    this.cityName = in.readString();
    this.latitude = in.readString();
    this.longitude = in.readString();
    this.countryCode = in.readString();
  }


  public City(long id, String cityName, String latitude, String longitude, String countryCode) {
    this.id = id;
    this.cityName = cityName;
    this.latitude = latitude;
    this.longitude = longitude;
    this.countryCode = countryCode;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(cityName);
    builder.append(", ");
    builder.append(countryCode);
    return builder.toString();
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(id);
    dest.writeString(cityName);
    dest.writeString(latitude);
    dest.writeString(longitude);
    dest.writeString(countryCode);
  }
}
