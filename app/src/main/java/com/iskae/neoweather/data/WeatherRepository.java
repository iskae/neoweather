package com.iskae.neoweather.data;

import com.iskae.neoweather.data.model.City;
import com.iskae.neoweather.data.model.WeatherData;
import com.iskae.neoweather.data.source.local.LocalWeatherRepository;
import com.iskae.neoweather.data.source.remote.RemoteWeatherRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * Created by iskae on 07.03.18.
 */

public class WeatherRepository {

  private LocalWeatherRepository localWeatherRepository;
  private RemoteWeatherRepository remoteWeatherRepository;

  @Inject
  public WeatherRepository(LocalWeatherRepository localWeatherRepository, RemoteWeatherRepository remoteWeatherRepository) {
    this.localWeatherRepository = localWeatherRepository;
    this.remoteWeatherRepository = remoteWeatherRepository;
  }

  public Flowable<List<City>> getCitiesList() {
    return localWeatherRepository.getCitiesList();
  }

  public void generateCitiesList() {
    localWeatherRepository.generateCitiesList();
  }

  public Maybe<WeatherData> getLastKnownWeatherByCityId(long cityId) {
    return localWeatherRepository.getLastKnownWeatherByCityId(cityId);
  }

  public void saveWeather(WeatherData weather) {
    localWeatherRepository.saveWeather(weather);
  }

  public Flowable<WeatherData> getCurrentWeatherByCityId(long cityId) {
    return remoteWeatherRepository.getCurrentWeatherByCityId(cityId);
  }
}
