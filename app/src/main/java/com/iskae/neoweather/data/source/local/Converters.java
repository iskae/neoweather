package com.iskae.neoweather.data.source.local;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.arch.persistence.room.TypeConverter;

import com.iskae.neoweather.data.model.Weather;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Created by iskae on 16.02.18.
 */

public class Converters {
  private static Gson gson = new Gson();

  @TypeConverter
  public static List<Weather> stringToWeatherList(String data) {
    if (data == null) {
      return Collections.emptyList();
    }
    Type listType = new TypeToken<List<Weather>>() {
    }.getType();

    return gson.fromJson(data, listType);
  }

  @TypeConverter
  public static String weatherListToString(List<Weather> weathers) {
    return gson.toJson(weathers);
  }

}
