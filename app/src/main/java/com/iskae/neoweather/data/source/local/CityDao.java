package com.iskae.neoweather.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.iskae.neoweather.data.model.City;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by iskae on 09.03.18.
 */
@Dao
public interface CityDao {
  @Query("SELECT * FROM City")
  Flowable<List<City>> getCitiesList();

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void insertCities(List<City> cities);
}
