package com.iskae.neoweather.data.source.remote;

import com.iskae.neoweather.BuildConfig;
import com.iskae.neoweather.data.model.WeatherData;

import javax.inject.Inject;

import io.reactivex.Flowable;

/**
 * Created by iskae on 09.03.18.
 */

public class RemoteWeatherRepository {
  private WeatherService service;

  @Inject
  public RemoteWeatherRepository(WeatherService service) {
    this.service = service;
  }

  public Flowable<WeatherData> getCurrentWeatherByCityId(long cityId) {
    return service.getCurrentWeatherForCoordinates(BuildConfig.API_KEY, String.valueOf(cityId));
  }
}
