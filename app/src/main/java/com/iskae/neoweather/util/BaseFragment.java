package com.iskae.neoweather.util;

import android.app.Activity;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * Created by iskae on 07.03.18.
 */

public class BaseFragment extends Fragment {

  @Inject
  protected ViewModelProvider.Factory viewModelFactory;

  public BaseFragment() {
  }

  @SuppressWarnings("deprecation")
  @Override
  public void onAttach(Activity activity) {
    AndroidSupportInjection.inject(this);
    super.onAttach(activity);
  }

  protected <T extends ViewModel> T getViewModel(final Class<T> cls) {
    return ViewModelProviders.of(this, viewModelFactory).get(cls);
  }
}
