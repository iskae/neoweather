package com.iskae.neoweather.util;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.iskae.neoweather.data.WeatherRepository;
import com.iskae.neoweather.data.model.City;
import com.iskae.neoweather.data.model.WeatherData;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by iskae on 07.03.18.
 */

public class SharedViewModel extends ViewModel {

  private final CompositeDisposable disposables = new CompositeDisposable();
  private final MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
  private final MutableLiveData<List<City>> citiesList = new MutableLiveData<>();
  private final MutableLiveData<WeatherData> currentWeather = new MutableLiveData<>();
  private final MutableLiveData<String> filter = new MutableLiveData<>();
  private final MutableLiveData<Throwable> error = new MutableLiveData<>();
  private WeatherRepository weatherRepository;

  @Inject
  public SharedViewModel(WeatherRepository weatherRepository) {
    this.weatherRepository = weatherRepository;
  }

  @Override
  protected void onCleared() {
    disposables.clear();
  }

  public LiveData<Boolean> getLoadingStatus() {
    return isLoading;
  }

  public LiveData<List<City>> getCitiesList() {
    return citiesList;
  }

  public LiveData<Throwable> getError() {
    return error;
  }

  public LiveData<WeatherData> getCurrentWeather() {
    return currentWeather;
  }

  public LiveData<String> getFilter() {
    return filter;
  }

  public void setFilter(String newFilter) {
    filter.setValue(newFilter);
  }

  public void loadCitiesList() {
    disposables.add(weatherRepository.getCitiesList()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe(s -> isLoading.setValue(true))
        .doOnTerminate(() -> isLoading.setValue(false))
        .subscribe(cities -> {
          if (cities.isEmpty()) {
            generateCitiesList();
          } else {
            citiesList.setValue(cities);
          }
        }, error::setValue)
    );
  }

  public void generateCitiesList() {
    Completable.fromAction(() ->
        weatherRepository.generateCitiesList()).observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io()).subscribe(new CompletableObserver() {
      @Override
      public void onSubscribe(Disposable d) {
      }

      @Override
      public void onComplete() {
      }

      @Override
      public void onError(Throwable e) {
        error.setValue(e);
      }
    });
  }

  public void loadWeatherByCityId(long cityId) {
    disposables.add(weatherRepository.getCurrentWeatherByCityId(cityId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe(s -> isLoading.setValue(true))
        .doOnTerminate(() -> isLoading.setValue(false))
        .subscribe(weather -> {
          if (weather != null) {
            saveLastKnownWeatherForCity(weather);
            currentWeather.setValue(weather);
          }
        }, error::setValue)
    );
  }

  public void loadLastKnownWeatherByCityId(long cityId) {
    disposables.add(weatherRepository.getLastKnownWeatherByCityId(cityId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe(s -> isLoading.setValue(true))
        .doOnComplete(() -> currentWeather.setValue(null))
        .subscribe(currentWeather::setValue, error::setValue)
    );
  }

  private void saveLastKnownWeatherForCity(WeatherData weather) {
    Completable.fromAction(() ->
        weatherRepository.saveWeather(weather)).observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io()).subscribe(new CompletableObserver() {
      @Override
      public void onSubscribe(Disposable d) {
      }

      @Override
      public void onComplete() {
      }

      @Override
      public void onError(Throwable e) {
        error.setValue(e);
      }
    });
  }
}
