package com.iskae.neoweather.util;

import android.app.Activity;
import android.app.Application;

import com.iskae.neoweather.BuildConfig;
import com.iskae.neoweather.di.ApplicationComponent;
import com.iskae.neoweather.di.ApplicationModule;
import com.iskae.neoweather.di.DaggerApplicationComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import timber.log.Timber;

/**
 * Created by iskae on 08.03.18.
 */

public class NeoWeatherApplication extends Application implements HasActivityInjector {
  @Inject
  DispatchingAndroidInjector<Activity> activityInjector;

  @Override
  public AndroidInjector<Activity> activityInjector() {
    return activityInjector;
  }

  @Override
  public void onCreate() {
    super.onCreate();

    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
    }

    component().inject(this);
  }

  protected ApplicationComponent component() {
    return DaggerApplicationComponent.builder()
        .applicationModule(new ApplicationModule(this))
        .build();
  }
}
