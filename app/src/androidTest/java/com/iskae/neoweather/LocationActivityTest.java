package com.iskae.neoweather;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.iskae.neoweather.search.SearchLocationsActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by iskae on 07.03.18.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class LocationActivityTest {
  @Rule
  public ActivityTestRule<SearchLocationsActivity> locationActivityTestRule =
      new ActivityTestRule<>(SearchLocationsActivity.class);


  @Test
  public void cityClickOpensWeatherViewTest() {
    onView(withId(R.id.citiesListView)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
    onView(withId(R.id.weatherContentView)).check(matches(isDisplayed()));
  }
}
