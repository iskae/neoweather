package com.iskae.neoweather;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.iskae.neoweather.data.source.remote.RemoteWeatherRepository;
import com.iskae.neoweather.data.source.remote.WeatherService;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

/**
 * Created by iskae on 09.03.18.
 */

@RunWith(JUnit4.class)
public class RemoteRepositoryTest {

  @Rule
  public MockitoRule rule = MockitoJUnit.rule();

  @Mock
  private WeatherService weatherService;

  @InjectMocks
  private RemoteWeatherRepository weatherRepository;

  @Before
  public void init() {
    weatherService = Mockito.mock(WeatherService.class);
    weatherRepository = new RemoteWeatherRepository(weatherService);
  }

  @Test
  public void emptyTest() {
    weatherService.getCurrentWeatherForCoordinates(BuildConfig.API_KEY, "2950159");//Berlin
  }

  @Test
  public void getCurrentWeatherTest() {
    weatherService.getCurrentWeatherForCoordinates(BuildConfig.API_KEY, "2950159");
    verify(weatherService, times(1)).getCurrentWeatherForCoordinates(BuildConfig.API_KEY, "2950159");
  }
}

